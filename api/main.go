package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
)

type product struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Image       string `json:"image"`
	Price       string `json:"price"`
	Specs       []spec `json:"specs"`
}

type spec string

var data = []product{
	product{
		ID:          1,
		Title:       "Petzl Gri Gri",
		Description: "Assisted braking belay device",
		Image:       "https://www.petzl.com/sfc/servlet.shepherd/version/download/0681r000004zPMJAA2",
		Price:       "109.99",
		Specs: []spec{
			spec("Optimized for 8.9-10.5 mm rope diameters"),
			spec("Rope installation diagram is engraved on both the interior and exterior of the device"),
			spec("Ergonomic handle and progressive cam action"),
			spec("Aluminum side plates, stainless-steel cam and friction plate and a nylon handle"),
			spec("Certified to CE EN 15151-1 and UIAA standards"),
		},
	},
	product{
		ID:          2,
		Title:       "Black Diamond Positron Quickdraw",
		Description: "12cm quickdraw with positron carabiners",
		Image:       "https://www.blackdiamondequipment.com/dw/image/v2/AAKN_PRD/on/demandware.static/-/Sites-bdel/default/dw0ca419ec/products/carabiners_draws/S16/381077_YELO_Positron_Quickdraw_12_cm_web.jpg?sw=472",
		Price:       "16.99",
		Specs: []spec{
			spec("2 Positron carabiners and a polyester dog bone"),
			spec("Straight gate Positron on top and bent gate Positron on the bottom"),
			spec("Rubber Straightjacket™ insert"),
		},
	},
	product{
		ID:          3,
		Title:       "Scarpa Instinct VS",
		Description: "Aggresive men's climbing shoes",
		Image:       "https://www.scarpa.com/media/catalog/product/cache/image/1400x1112/e9c3970ab036de70892d86c6d221abfe/I/N/INSTICT%20VS%20BLACK_ORANGE_EST_IPPS.jpg",
		Price:       "185.00",
		Specs: []spec{
			spec("Uppers made of Lorica® synthetic leather"),
			spec("Tensioned rand design"),
			spec("Rubber patches on the tops of the toes"),
			spec("Vibram XS Edge rubber"),
			spec("Shoes can be resoled"),
		},
	},
	product{
		ID:          4,
		Title:       "Black Diamond Momentum Harness",
		Description: "Men's climbing harness",
		Image:       "https://www.blackdiamondequipment.com/dw/image/v2/AAKN_PRD/on/demandware.static/-/Sites-bdel/default/dwdf7d50cd/products/S18_Product_Images/Equipment/651075_SLAT_Slate_Momentum-Mens_Main.jpg?sw=472",
		Price:       "59.99",
		Specs: []spec{
			spec("Bullhorn-shaped waistbelt"),
			spec("Adjustable rear elastic riser"),
			spec("Haul loop and 4 pressure-molded gear loops"),
		},
	},
}

func main() {
	portPtr := flag.Int("p", 8080, "The port to run the API on")
	flag.Parse()

	h := http.NewServeMux()

	h.HandleFunc("/products", getData)
	log.Printf("Starting server on port %d...", *portPtr)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", *portPtr), h); err != nil {
		panic(err)
	}
}

func getData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	if r.Method == "OPTIONS" {
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		json.NewEncoder(w).Encode(map[string]string{"message": "method not supported"})
		return
	}

	json.NewEncoder(w).Encode(data)
}
