/**
   * Gets a list of products from the API.
   * @name getProducts
   * @returns {Object}
   */
async function getProducts() {
  // TODO: Add code to retrieve products from https://api.mcgit.cc/mcg/interview/products
}

/**
 * A utility class for generating a product list.
 * @class
 */
var ProductListBuilder = (function() {
  "use strict";
  /**
   * Initializes a new instance of ProductListBuilder with a root node to build the cart list under.
   * @constructs ProductListBuilder 
   */
  function ProductListBuilder(rootNode, products) {
    this.rootNode = rootNode;
    this.products = products;
  }

  /**
   * Generates and displays a list of products.
   * @name ProductListBuilder#build
   * @returns {void}
   */
  ProductListBuilder.prototype.build = function() {
    for (let i = 0; i < this.products.length; i++) {
      const prod = this.products[i];
      // TODO: Add code to generate list of items according to the structure in the README
    }
  };

  return ProductListBuilder;
})();

$(document).ready(async function() {
  // This assumes you are using promises although they are not required.
  let products = await getProducts();
  
  let rootNode = document.getElementById("product-list");
  (new ProductListBuilder(rootNode, products)).build();
});
