# Interview Code Sample

Your objective with this small coding project is to consume a REST API and dynamically produce a list of items with JavaScript. 

Begin by branching from master. The file `app/js/products.js` contains some boilerplate to get you started. Additionally, the `axios` and `jQuery` libraries are provided if you choose to use them. You can obtain the products by making a `GET` request to `https://api.mcgit.cc/mcg/interview/products`. List items should be placed within the div with ID `products-list`. Each list item should take the form

```html
<div class="product flex">
  <div>
    <img src="prod.image">
  </div>
  <div class="flex width-1">
    <div class="content flex flex-column">
        <h2>prod.title</h2>
        <h3>prod.description</h3>
        <ul>
          <li>prod.specs[0]</li>
          <li>prod.specs[1]</li>
          <li>prod.specs[2]</li>
        </ul>
    </div>
    <div class="price flex flex-column">
      <div>
        <p>$prod.price</p>
      </div>
      <div>
        <button>Add To Cart</button>
      </div>
    </div>
  </div>
</div>
```

After adding your code, running `index.html` in a browser should produce the following result:

![result](code_sample_result.png)

Finally, add an event listener to each `Add to Cart` button so that the cart count is incremented whenever a button is clicked. The number to increment is held within the `<h3>` with id `cart-count`.

When finished, submit a pull request on bitbucket.

You are permitted to use official documentation to complete this.